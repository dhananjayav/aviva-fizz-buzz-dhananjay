﻿// <copyright file="BusinessTest.cs" company="TCS">
// Copyright (c) company. All rights reserved.
// </copyright>
// <author>Dhananjay</author>
// <email>dhananjaya.v@tcs.com</email>
// <date>27-08-2014</date>
// <summary>BusinessTest class to test Business classes.</summary>
namespace FizzBuzz.Business.UnitTest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FizzBuzz.Business;
    using FizzBuzz.Models;
    using NUnit.Framework;

    /// <summary>
    /// BusinessTest class
    /// </summary>
    [TestFixture]
    public class BusinessTest
    {
        /// <summary>
        /// Test Get Fizz Buzz entities method.
        /// </summary>
        [Test]
        public void TestGetFizzBuzzEntities()
        {
            FizzBuzzBusiness fizzbuzzBusiness = new FizzBuzzBusiness();
            List<IFizzBuzz> fizzBuzzCollection = fizzbuzzBusiness.GetFizzBuzzEntities(3);
            Assert.AreEqual(3, fizzBuzzCollection.Count);
        }

        /// <summary>
        /// Test FizzBuzz Factory method.
        /// </summary>
        [Test]
        public void TestFizzBuzzFactory()
        {
            IFizzBuzz fizzBuzz = FizzBuzzFactory.GetFizzBuzzEntiy(3);
            if (DateTime.Now.DayOfWeek != DayOfWeek.Wednesday)
            {
                Assert.AreEqual("Fizz", fizzBuzz.FizzbuzzOutput);
            }
            else
            {
                Assert.AreEqual("Wizz", fizzBuzz.FizzbuzzOutput);
            }
        }
    }
}
