﻿// <copyright file="FizzBuzzController.cs" company="TCS">
// Copyright (c) company. All rights reserved.
// </copyright>
// <author>Dhananjay</author>
// <email>dhananjaya.v@tcs.com</email>
// <date>27-08-2014</date>
// <summary>FizzBuzzController class</summary>
namespace FizzBuzz.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using FizzBuzz.Business;
    using FizzBuzz.Models;
    using PagedList;
    using PagedList.Mvc;

    /// <summary>
    /// FizzBuzzController class
    /// </summary>
    public class FizzBuzzController : Controller
    {
        /// <summary>
        /// Fizz buzz Service field
        /// </summary>
        private IFizzBuzzBusiness fizzbuzzService;

        /// <summary>
        /// State Provider field
        /// </summary>
        private IStateProvider stateProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzController"/> class
        /// </summary>
        /// <param name="fizzbuzzService">FizzBuzz service instance</param>
        /// <param name="stateProvider">State provider instance</param>
        public FizzBuzzController(IFizzBuzzBusiness fizzbuzzService, IStateProvider stateProvider)
        {
            this.fizzbuzzService = fizzbuzzService;
            this.stateProvider = stateProvider;
        }

        /// <summary>
        /// Index action method.
        /// </summary>
        /// <returns>Return ActionResult</returns>
        public ActionResult Index()
        {
            return this.View();
        }

        /// <summary>
        /// Index post method.
        /// </summary>
        /// <param name="fizzbuzzInput">Fizz buzz Input Item</param>
        /// <returns>ActionResult Item</returns>
        [HttpPost]
        public ActionResult Index(FizzBuzzInput fizzbuzzInput)
        {
            if (ModelState.IsValid)
            {
                if (fizzbuzzInput == null)
                {
                    return this.View("Index");
                }

                if (fizzbuzzInput.EnteredNumber <= 0 || fizzbuzzInput.EnteredNumber > 1000)
                {
                    return this.View("Index");
                }

                this.stateProvider["FbEntities"] = this.fizzbuzzService.GetFizzBuzzEntities(fizzbuzzInput.EnteredNumber);
                return this.View("Details", ((List<IFizzBuzz>)this.stateProvider["FbEntities"]).ToPagedList(1, 20));
            }

            return this.View("Index");
        }

        /// <summary>
        /// Details method
        /// </summary>
        /// <param name="page">page item</param>
        /// <returns>Action Result Item</returns>
        public ActionResult Details(int? page)
        {
            if (page == null || page.Value <= 0)
            {
                return this.RedirectToAction("Index");
            }

            int pageSize = 20;
            int pageNumber = page ?? 1;
            return this.View("Details", ((List<IFizzBuzz>)this.stateProvider["FbEntities"]).ToPagedList(pageNumber, pageSize));
        }
    }
}
