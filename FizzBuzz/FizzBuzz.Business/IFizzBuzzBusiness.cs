﻿// <copyright file="IFizzBuzzBusiness.cs" company="TCS">
// Copyright (c) company. All rights reserved.
// </copyright>
// <author>Dhananjay</author>
// <email>dhananjaya.v@tcs.com</email>
// <date>27-08-2014</date>
// <summary>IFizzBuzzBusiness interface would be input to controller.</summary>
namespace FizzBuzz.Business
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FizzBuzz.Models;

    /// <summary>
    /// IFizzBuzz Business interface.
    /// </summary>
    public interface IFizzBuzzBusiness
    {
        /// <summary>
        /// This method returns Fizz Buzz entities. Should be implemented by child classes.
        /// </summary>
        /// <param name="enteredNumber">Number to get FizzBuzz entity</param>
        /// <returns>List of Fizz Buzz interface entity</returns>
        List<FizzBuzz.Models.IFizzBuzz> GetFizzBuzzEntities(int enteredNumber);
    }
}
