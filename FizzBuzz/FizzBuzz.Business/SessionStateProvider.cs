﻿// <copyright file="SessionStateProvider.cs" company="TCS">
// Copyright (c) company. All rights reserved.
// </copyright>
// <author>Dhananjay</author>
// <email>dhananjaya.v@tcs.com</email>
// <date>27-08-2014</date>
// <summary>SessionStateProvider to create a class to hold the state</summary>
namespace FizzBuzz.Business
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// Session state provider
    /// </summary>
    public class SessionStateProvider : IStateProvider
    {
        /// <summary>
        /// Gets or Sets the objects
        /// </summary>
        /// <param name="key">key item</param>
        /// <returns>value item</returns>
        public object this[string key]
        {
            get
            {
                return HttpContext.Current.Session[key];
            }

            set
            {
                HttpContext.Current.Session[key] = value;
            }
        }

        /// <summary>
        /// Removes the object from the session.
        /// </summary>
        /// <param name="key">key item</param>
        public void Remove(string key)
        {
            HttpContext.Current.Session.Remove(key);
        }
    }
}