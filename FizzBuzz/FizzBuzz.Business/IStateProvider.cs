﻿// <copyright file="IStateProvider.cs" company="TCS">
// Copyright (c) company. All rights reserved.
// </copyright>
// <author>Dhananjay</author>
// <email>dhananjaya.v@tcs.com</email>
// <date>27-08-2014</date>
// <summary>IStateProvider to create a class to hold the state</summary>
namespace FizzBuzz.Business
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// IStateProvider interface
    /// </summary>
    public interface IStateProvider
    {
        /// <summary>
        /// Gets or Sets the input object.
        /// </summary>
        /// <param name="key">key item of type string</param>
        /// <returns>value item</returns>
        object this[string key] { get; set; }

        /// <summary>
        /// Removes the value item. 
        /// </summary>
        /// <param name="key">key item</param>
        void Remove(string key);
    }
}
