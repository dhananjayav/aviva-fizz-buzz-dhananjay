﻿// <copyright file="FizzBuzzBusiness.cs" company="TCS">
// Copyright (c) company. All rights reserved.
// </copyright>
// <author>Dhananjay</author>
// <email>dhananjaya.v@tcs.com</email>
// <date>27-08-2014</date>
// <summary>FizzBuzzBusiness class implements IFizzBuzz interface.</summary>
namespace FizzBuzz.Business
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FizzBuzz.Models;

    /// <summary>
    /// FizzBuzzBusiness class
    /// </summary>
    public class FizzBuzzBusiness : IFizzBuzzBusiness
    {
        /// <summary>
        /// Returns FizzBuzz entities.
        /// </summary>
        /// <param name="enteredNumber">integer number</param>
        /// <returns>List of FizzBuzz entities based on the number</returns>
        public List<Models.IFizzBuzz> GetFizzBuzzEntities(int enteredNumber)
        {
            List<IFizzBuzz> fizzbuzzList = new List<IFizzBuzz>();
            for (int i = 1; i <= enteredNumber; i++)
            {
                fizzbuzzList.Add(FizzBuzzFactory.GetFizzBuzzEntiy(i));
            }

            return fizzbuzzList;
        }
    }
}
