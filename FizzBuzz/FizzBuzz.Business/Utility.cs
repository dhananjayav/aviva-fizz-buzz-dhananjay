﻿// <copyright file="Utility.cs" company="TCS">
// Copyright (c) company. All rights reserved.
// </copyright>
// <author>Dhananjay</author>
// <email>dhananjaya.v@tcs.com</email>
// <date>27-08-2014</date>
// <summary>Utility class</summary>
namespace FizzBuzz.Business
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Utility class
    /// </summary>
    public static class Utility
    {
        /// <summary>
        /// FizzBuzz string based on the day
        /// </summary>
        /// <param name="inputString">Fizz Buzz string</param>
        /// <returns>String based on the day</returns>
        public static string GetStringOnDay(string inputString)
        {
            if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
            {
                inputString = inputString.Replace('F', 'W').Replace('B', 'W');
            }

            return inputString;
        }
    }
}
