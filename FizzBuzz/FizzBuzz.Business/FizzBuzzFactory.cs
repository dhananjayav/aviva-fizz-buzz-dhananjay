﻿// <copyright file="FizzBuzzFactory.cs" company="TCS">
// Copyright (c) company. All rights reserved.
// </copyright>
// <author>Dhananjay</author>
// <email>dhananjaya.v@tcs.com</email>
// <date>27-08-2014</date>
// <summary>FizzBuzzFactory class to create model class depending on input</summary>
namespace FizzBuzz.Business
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FizzBuzz.Models;

    /// <summary>
    /// FizzBuzzFactory static class.
    /// </summary>
    public static class FizzBuzzFactory
    {
        /// <summary>
        /// Factory method to get the FizzBuzz entity
        /// </summary>
        /// <param name="number">individual number to get Fizz Buzz entity</param>
        /// <returns>Gets IFizzBuzz entity</returns>
        public static IFizzBuzz GetFizzBuzzEntiy(int number)
        {
            if (number % 3 == 0 && number % 5 == 0)
            {
                return new FizzBuzzEntity(number) { Number = number, FizzbuzzOutput = Utility.GetStringOnDay("Fizz Buzz") };
            }
            else if (number % 5 == 0)
            {
                return new BuzzEntity(number) { Number = number, FizzbuzzOutput = Utility.GetStringOnDay("Buzz") };
            }
            else if (number % 3 == 0)
            {
                return new FizzEntity(number) { Number = number, FizzbuzzOutput = Utility.GetStringOnDay("Fizz") };
            }
            else
            {
                return new NumberEntity(number) { Number = number, FizzbuzzOutput = number.ToString() };
            }
        }
    }
}
