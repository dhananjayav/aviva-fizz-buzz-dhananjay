﻿// <copyright file="DictionaryStateProvider.cs" company="TCS">
// Copyright (c) company. All rights reserved.
// </copyright>
// <author>Dhananjay</author>
// <email>dhananjaya.v@tcs.com</email>
// <date>27-08-2014</date>
// <summary>FizzBuzzBusiness class implements IFizzBuzz interface.</summary>
namespace FizzBuzz.Controllers.UnitTest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FizzBuzz.Business;

    /// <summary>
    /// Class to mock Session state
    /// </summary>
    public class DictionaryStateProvider : IStateProvider
    {
        /// <summary>
        /// State Value collection parameter.
        /// </summary>
        private Dictionary<string, object> stateValueCollection =
            new Dictionary<string, object>();

        /// <summary>
        /// Method to get the item based on key.
        /// </summary>
        /// <param name="key">string parameter</param>
        /// <returns>Method returns an object in the dictionary</returns>
        public object this[string key]
        {
            get
            {
                if (this.stateValueCollection.ContainsKey(key))
                {
                    return this.stateValueCollection[key];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                this.stateValueCollection[key] = value;
            }
        }

        /// <summary>
        /// Method to add the item to collection
        /// </summary>
        /// <param name="key">key to be removed</param>
        public void Remove(string key)
        {
            this.stateValueCollection.Remove(key);
        }
    }
}
