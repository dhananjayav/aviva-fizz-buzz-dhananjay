﻿// <copyright file="FizzBuzzControllerTest.cs" company="TCS">
// Copyright (c) company. All rights reserved.
// </copyright>
// <author>Dhananjay</author>
// <email>dhananjaya.v@tcs.com</email>
// <date>27-08-2014</date>
// <summary>FizzBuzzBusiness class implements IFizzBuzz interface.</summary>
namespace FizzBuzz.Controllers.UnitTest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using FizzBuzz.Business;
    using FizzBuzz.Controllers;
    using FizzBuzz.Models;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// FizzBuzzControllerTest class
    /// </summary>
    public class FizzBuzzControllerTest
    {
        /// <summary>
        /// Dictionary state provider field.
        /// </summary>
        private DictionaryStateProvider dictionaryStateProvider = null;

        /// <summary>
        /// Fizz Buzz Controller test field.
        /// </summary>
        private FizzBuzzController fizzbuzzController = null;

        /// <summary>
        /// Setup method
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            var fizzbuzzMock = new Mock<IFizzBuzzBusiness>();
            fizzbuzzMock.Setup(r => r.GetFizzBuzzEntities(3)).Returns(
                new List<IFizzBuzz>()
                {
                new NumberEntity(1) { Number = 1, FizzbuzzOutput = "1" },
                new NumberEntity(2) { Number = 2, FizzbuzzOutput = "2" },
                new NumberEntity(3) { Number = 3, FizzbuzzOutput = "Fizz" }
                });

            this.dictionaryStateProvider = new DictionaryStateProvider();
            this.fizzbuzzController = new FizzBuzzController(fizzbuzzMock.Object, this.dictionaryStateProvider);
        }

        /// <summary>
        /// Method to test null input for number
        /// </summary>
        [Test]
        public void TestIfInputNumberisNull()
        {
            ViewResult vr = this.fizzbuzzController.Index(null) as ViewResult;
            Assert.AreEqual("Index", vr.ViewName);
        }

        /// <summary>
        /// Method to test valid inputs for entered number.
        /// </summary>
        [Test]
        public void TestIfInputNumberisValid()
        {
            FizzBuzzInput fizzbuzzInput = new FizzBuzzInput();
            fizzbuzzInput.EnteredNumber = 0;
            ViewResult vr = this.fizzbuzzController.Index(fizzbuzzInput) as ViewResult;
            Assert.AreEqual("Index", vr.ViewName);
        }

        /// <summary>
        /// Method to test Fizz Buzz output.
        /// </summary>
        [Test]
        public void TestFizzBuzz()
        {
            FizzBuzzInput fizzbizzInput = new FizzBuzzInput();
            fizzbizzInput.EnteredNumber = 3;
            ViewResult vr = this.fizzbuzzController.Index(fizzbizzInput) as ViewResult;
            Assert.AreEqual("Details", vr.ViewName);
        }

        /// <summary>
        /// Method to test the pagination output.
        /// </summary>
        [Test]
        public void TestFizzBuzzWithPagination()
        {
            FizzBuzzInput fb = new FizzBuzzInput();
            RedirectToRouteResult ar = this.fizzbuzzController.Details(null) as RedirectToRouteResult;
            Assert.AreEqual("Index", ar.RouteValues["action"].ToString()); 
        }
    }
}
