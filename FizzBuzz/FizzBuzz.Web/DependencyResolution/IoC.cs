using StructureMap;
using FizzBuzz.Business;

namespace FizzBuzz.Web {
    public static class IoC {
        public static IContainer Initialize() {
            ObjectFactory.Initialize(x =>
                        {
                            x.Scan(scan =>
                                    {
                                        scan.TheCallingAssembly();
                                        scan.WithDefaultConventions();
                                    });
                            x.For<IFizzBuzzBusiness>().Use<FizzBuzzBusiness>();
                            x.For<IStateProvider>().Use<SessionStateProvider>();
                        });
            return ObjectFactory.Container;
        }
    }
}