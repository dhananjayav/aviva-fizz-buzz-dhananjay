﻿// <copyright file="BuzzEntity.cs" company="TCS">
// Copyright (c) company. All rights reserved.
// </copyright>
// <author>Dhananjay</author>
// <email>dhananjaya.v@tcs.com</email>
// <date>27-08-2014</date>
// <summary>BuzzEntity class when the number is divisble by 5</summary>
namespace FizzBuzz.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// BuzzEntity class
    /// </summary>
    public class BuzzEntity : IFizzBuzz
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BuzzEntity"/> class
        /// </summary>
        /// <param name="number">Entered Number</param>
        public BuzzEntity(int number)
        {
            this.Number = number;
        }

        /// <summary>
        /// Gets or sets Number property
        /// </summary>
        public int Number
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Fizz Buzz output property
        /// </summary>
        public string FizzbuzzOutput
        {
            get;
            set;
        }
    }
}
