﻿// <copyright file="FizzEntity.cs" company="TCS">
// Copyright (c) company. All rights reserved.
// </copyright>
// <author>Dhananjay</author>
// <email>dhananjaya.v@tcs.com</email>
// <date>27-08-2014</date>
// <summary>FizzEntity class when the number is divisble by three</summary>
namespace FizzBuzz.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Fizz Buzz Entity class
    /// </summary>
    public class FizzEntity : IFizzBuzz
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FizzEntity"/> class
        /// </summary>
        /// <param name="number">number item</param>
        public FizzEntity(int number)
        {
            this.Number = number;
        }

        /// <summary>
        /// Gets or sets number field
        /// </summary>
        public int Number
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Fizz buzz output
        /// </summary>
        public string FizzbuzzOutput
        {
            get;
            set;
        }
    }
}
