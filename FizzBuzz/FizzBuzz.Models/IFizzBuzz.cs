﻿// <copyright file="IFizzBuzz.cs" company="TCS">
// Copyright (c) company. All rights reserved.
// </copyright>
// <author>Dhananjay</author>
// <email>dhananjaya.v@tcs.com</email>
// <date>27-08-2014</date>
// <summary>IFizzBuzz interface</summary>
namespace FizzBuzz.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for Fizz Buzz model.
    /// </summary>
    public interface IFizzBuzz
    {
        /// <summary>
        /// Gets or sets individual fizz buzz number
        /// </summary>
        int Number
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Fizz Buzz Output
        /// </summary>
        string FizzbuzzOutput
        {
            get;
            set;
        }
    }
}
