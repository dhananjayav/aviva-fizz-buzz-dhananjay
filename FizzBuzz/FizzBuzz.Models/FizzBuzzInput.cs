﻿// <copyright file="FizzBuzzInput.cs" company="TCS">
// Copyright (c) company. All rights reserved.
// </copyright>
// <author>Dhananjay</author>
// <email>dhananjaya.v@tcs.com</email>
// <date>27-08-2014</date>
// <summary>FizzBuzzInput model class used to gather the input screen</summary>
namespace FizzBuzz.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// FizzBuzz Input class.
    /// </summary>
    public class FizzBuzzInput
    {
        /// <summary>
        /// Entered Number field.
        /// </summary>
        private int enteredNumber;

        /// <summary>
        /// Gets or sets entered Number property and validations.
        /// </summary>
        [Required(ErrorMessage = "Number is mandatory.")]
        [Range(1, 1000)]
        [RegularExpression(@"[0-9]*")]
        public int EnteredNumber
        {
            get
            {
                return this.enteredNumber;
            }

            set
            {
                this.enteredNumber = value;
            }
        }
    }
}
