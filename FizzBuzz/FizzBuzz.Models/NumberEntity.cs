﻿// <copyright file="NumberEntity.cs" company="TCS">
// Copyright (c) company. All rights reserved.
// </copyright>
// <author>Dhananjay</author>
// <email>dhananjaya.v@tcs.com</email>
// <date>27-08-2014</date>
// <summary>NumberEntity class when the number is not divisble by 3 or 5</summary>
namespace FizzBuzz.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// NumberEntity class
    /// </summary>
    public class NumberEntity : IFizzBuzz
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NumberEntity"/> class
        /// </summary>
        /// <param name="number">number item</param>
        public NumberEntity(int number)
        {
            this.Number = number;
        }

        /// <summary>
        /// Gets or sets number
        /// </summary>
        public int Number
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets output
        /// </summary>
        public string FizzbuzzOutput
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Font color
        /// </summary>
        public string FontColor
        {
            get;
            set;
        }
    }
}
