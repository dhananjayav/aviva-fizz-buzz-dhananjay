﻿// <copyright file="FizzBuzzEntity.cs" company="TCS">
// Copyright (c) company. All rights reserved.
// </copyright>
// <author>Dhananjay</author>
// <email>dhananjaya.v@tcs.com</email>
// <date>27-08-2014</date>
// <summary>FizzBuzzEntity class when the number is divisble by 3 and 5</summary>
namespace FizzBuzz.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// FizzBuzzEntity class
    /// </summary>
    public class FizzBuzzEntity : IFizzBuzz
    {
        /// <summary>
        /// Number field
        /// </summary>
        private int number;

        /// <summary>
        /// Fizz Buzz output.
        /// </summary>
        private string fizzbuzzOutput;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzEntity"/> class
        /// </summary>
        /// <param name="inputNumber">Number item</param>
        public FizzBuzzEntity(int inputNumber)
        {
            this.Number = inputNumber;
        }

        /// <summary>
        /// Gets or sets number field.
        /// </summary>
        public int Number
        {
            get { return this.number; }
            set { this.number = value; }
        }

        /// <summary>
        /// Gets or sets Fizz Buzz output property.
        /// </summary>
        public string FizzbuzzOutput
        {
            get
            {
                return this.fizzbuzzOutput;
            }

            set
            {
                this.fizzbuzzOutput = value;
            }
        }
    }
}
